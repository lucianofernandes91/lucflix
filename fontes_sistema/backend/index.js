const express = require('express');
const app = express();

const fs = require('fs');

const FDS = require('./video_streaming')

app.get('/', (req, res) => {

    // Função secundária de envio da página HOME do site
    function envia_pagina_home() {
        fs.readFile('../frontend/index.html', (err, html) => res.end(html));
    }

    // Página Principal (index.html)
    envia_pagina_home();
});

// API que retorna vídeo a ser 'streemado'
app.get('/videos/:nomeVideo', (req, res) => {

    // Função secundária - Prepara as variáveis para achamada da função principal de envio
    function prepara_variaveis() {

        /// Compoe o nome do arquivo e o coloca na variável arquivo_do_video
        const { nome_do_video } = req.params;
        var dirAtual = __dirname;
        const arquivo_do_video = `${dirAtual}\\videos\\${nome_do_video}`;
    }

    // Função secundária - Preparo e envio do arquivo de vídeo    
    function envia_video() {

        /// Prepara as variáveis
        prepara_variaveis();

        /// Chamada da função principal de envio do vídeo
        envia_video_streaming(arquivo_do_video, req, res);
    }

    // Rotina Principal
    envia_video();
})

app.listen(80, () => console.log('VideoFlix Server!'));