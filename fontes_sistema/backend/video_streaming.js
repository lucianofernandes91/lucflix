const fs = require('fs');

//////////////////////////////////////////////////////////////////
// FUNCTION ENVIA_VIDEO_STREAMING(arquivo_para_stream, reQ, res) //
/////////////////////////////////////////////////////////////////
// ENTRADA: 
// -- arquivo (path) a ser enviado;
// -- 'req' e 'res' vindos da rota (GET) do Express.
//
// DESCRIÇÃO:
// - Recebe nome do arquivo de vídeo (path completo) e os parâmetros req/res da rota chamada no express e
//   Envia tal arquivo pra quem o requisitou (req, res).
//
// EXEMPLO DE USO:
//
// app.get('/starwars'), (req, res) => {
//     envia_video_streaming('c:\star_wars_735354.mp4', req, res);
// }
//
function envia_video_streaming(arquivo_para_stream, req, res) {
    // Funcção secundária para gerar erro em caso de falha
    function gera_erro() {
        console.log(err);
        return res.status(404).end('<h1>Video não encontrado.</h1>');
    }

    // Função secundária final - enviar o vídeo via streaming
    function envia_stream() {
        // Variáveis necessárias para montar o chunk header corretamente
        const { range } = req.headers;
        const { size } = stats;
        const start = Number((range || '').replace(/bytes=/, '').split('-')[0]);
        const end = size - 1;
        const chunkSize = (end - start) + 1;

        // Definindo headers de chunk
        res.set({
            'Content-Range': `bytes ${start}-${end}/${size}`,
            'Accept-Ranges': 'bytes',
            'Content-Length': chunkSize,
            'Content-Type': 'video/mp4'
        });

        // É importante usar status 206 - Partial Content para o streaming funcionar
         res.status(206);
         // Utilizando ReadStream do Node.js
         // Ele vai ler um arquivo e enviá-lo em partes via stream.pipe()
         const stream = fs.createReadStream(movieFile, { start, end });
         stream.on('open', () => stream.pipe(res));
         stream.on('error', (streamErr) => res.end(streamErr));
    };

    // Rotina Principal
    fs.stat(arquivo_para_stream, (err, stats) => {
        if (err) {
            gera_erro();
        } else {
            envia_stream();
        }
    }); 
}   
